import scala.collection.mutable.HashMap
import scala.io.StdIn._

class Variable(var content: Int)

class Environment(var extension: Environment = null) extends HashMap[String, Variable] {

  put("return", new Variable(0))

  override def apply(name: String): Variable = {
    if (contains(name)) super.apply(name)
    else if (extension != null) extension(name)
    else throw new Exception("undefined variable: " + name)
  }
}


object processor {

  var currentEnv = new Environment
  var program: Array[String] = null
  var ip = 0
  var ir: Array[String] = null
  val labels = new HashMap[String, Int]

  // initialize labels
  def preProcess(fileName: String) {
    program = io.Source.fromFile(fileName).getLines.toArray
    for (i <- 0 until program.length) {
      if (!program(i).isEmpty()) {
        ir = program(i).split("\\s+")
        if (ir(0) == "label:") labels.put(ir(1), i)
      }
    }
  }

  // useful for converting variables or literals to ints:
  def get(term: String): Int = if (term.matches("\\d+")) term.toInt else currentEnv(term).content


  def fetchExecute() {
    var halt = false
    ip = 0
    while (!halt) {
      try {
        ir = program(ip).split("\\s+")
        ip = ip + 1
        ir(0) match {
          // Arithmetic and logic
          case "add" => currentEnv(ir(1)).content = get(ir(2)) + get(ir(3))
          case "sub" => currentEnv(ir(1)).content = get(ir(2)) - get(ir(3))
          case "mul" => currentEnv(ir(1)).content = get(ir(2)) * get(ir(3))
          case "div" => currentEnv(ir(1)).content = get(ir(2)) / get(ir(3))
          case "and" => currentEnv(ir(1)).content = if (get(ir(2)) == 0 || get(ir(3)) == 0) 0 else if (get(ir(2)) < 0 || get(ir(3)) < 0) -1 else 1
          case "or" => currentEnv(ir(1)).content = if (get(ir(2)) > 0 || get(ir(3)) > 0) 1 else if (get(ir(2)) < 0 || get(ir(3)) < 0) -1 else 0
          case "equal" => currentEnv(ir(1)).content = if (get(ir(2)) == get(ir(3))) 1 else if (get(ir(2)) < 0 || get(ir(3)) < 0) -1 else 0
          case "not" => currentEnv(ir(1)).content = if (get(ir(2)) > 0) 0 else if (get(ir(2)) == 0) 1 else -1

          // opcodes
          case "def" => currentEnv.put(ir(1), new Variable(get(ir(2))))
          case "load" => currentEnv(ir(1)).content = get(ir(2))
          case "if" => if (currentEnv(ir(1)).content != 0) ip = labels.getOrElseUpdate(ir(2), -1)
          case "goto" => ip = labels.getOrElseUpdate(ir(1), -1)
          case "read" => {
            print(ir(1) + " = ")
            currentEnv(ir(1)).content = readInt()
          }
          case "print" => println("=> " + get(ir(1)))
          case "halt" => halt = true
          case "printmsg" => for (i <- 1 until ir.length) print(ir(i) + " ")

          // for Amoeba 2.0
          case "call" => {
            currentEnv = new Environment(currentEnv)
            currentEnv.put("ip", new Variable(ip))
            ip = labels.getOrElseUpdate(ir(1), -1)
            for (i <- 2 until ir.length)
              currentEnv.put("arg" + (i - 2), new Variable(currentEnv.extension(ir(2)).content))
          }
          case "return" => {
            val result = currentEnv(ir(1)).content
            ip = currentEnv("ip").content
            currentEnv = currentEnv.extension
            currentEnv("return") = new Variable(result)
          }

          // no-ops
          case "label:" => {}
          case "comment:" => {}
          case "" => {}
          // etc.
          case _ => throw new Exception("unrecognized opcode: " + ir(0))
        }
      } catch {
        case e: Exception => println(e)
      }
    }
    println("bye ... ")
  }

  def main(args: Array[String]): Unit = {
    val programFile = readLine("Enter program name: ")
    preProcess(programFile)
    fetchExecute
  }
}
