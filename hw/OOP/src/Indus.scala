import scala.collection.mutable.ArrayBuffer

object Indus extends App {
  val inventory = new ArrayBuffer[Item]

  for (i <- 1 to 5) inventory += Item(Description("The Matrix DVD", 15.5, "DVD World"))
  for (i <- 1 to 3) inventory += Item(Description("The Terminator DVD", 13.25, "DVD World"))
  for (i <- 1 to 2) inventory += Item(Description("Ironman DVD", 18, "DVD Planet"))

  println("Inventory size = " + inventory.size)
  println()
  for (item <- inventory) println("ID: " + item.getId + ", Item: " + item.toString())
  println()
  println(inventory)

}
