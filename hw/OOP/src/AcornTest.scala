

object AcornTest extends App {

  def execute(exp: Expression) {
    try {
      print(exp.toString + " = ")
      val value = exp.execute
      println(value.toString)
    } catch {
      case e: Exception => println(e)
    }
  }

  execute(Sum(Number(3.1), Number(4.2)))
  execute(And(Boole(true), Boole.TRUE))
  execute(Sum(Number(3.1), Number(2.1)))
  execute(Subtract(Number(3.1), Number(2.1)))
  execute(Sum(Sum(Number(3.1), Number(4.2)), Sum(Number(3.5), Number(2.8))))
  execute(Times(Number(10), Sum(Number(3), Number(5))))
  execute(Divide(Sum(Number(3.1), Number(4.2)), Sum(Number(3.5), Number(2.8))))
  execute(Less(Number(10), Sum(Number(3), Number(5))))
  execute(Less(Number(7), Sum(Number(3), Number(5))))
  execute(Not(Equal(Number(10), Sum(Number(3), Number(5)))))
  execute(Or(Not(And(Boole.TRUE, Boole.FALSE)), Equal(Number(3), Number(5))))
  execute(Equal(Number(8), Sum(Number(3), Number(5))))

  execute(Equal(Number(8), Boole.TRUE))
  execute(And(Boole.FALSE, Number(3.1)))
  execute(And(Number(3.1), Boole.FALSE))
  execute(Or(Number(10), Sum(Number(3), Number(5))))

}
