

class Equal(val operand1: Expression, val operand2: Expression) extends Expression {

  def execute = {
    val value1 = operand1.execute
    val value2 = operand2.execute
    if (value1.isInstanceOf[Boole] && value2.isInstanceOf[Number] || value1.isInstanceOf[Number] && value2.isInstanceOf[Boole])
      throw new Exception("type mismatch: operands must be of the same types.")
    else {
      if (value1.isInstanceOf[Boole]) {
        val num1 = value1.asInstanceOf[Boole]
        val num2 = value2.asInstanceOf[Boole]
        num1 == num2
      }
      else {
        val num1 = value1.asInstanceOf[Number]
        val num2 = value2.asInstanceOf[Number]
        num1 == num2
      }
    }
  }

  override def toString = "(" + operand1.toString + " == " + operand2.toString + ")"
}

object Equal {
  def apply(operand1: Expression, operand2: Expression) =
    new Equal(operand1, operand2)
}
