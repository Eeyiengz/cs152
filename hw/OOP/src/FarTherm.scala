

class FarTherm extends CenTherm with IThermometer {
  override def getMeanTemperature(cities: List[String]): Double = {
    var sum = 0.0
    for(city <- cities) sum += helper(computeTemp(city))
    if (cities.isEmpty) 0.0 else sum / cities.size
  }
  private def helper(cen: Double)  = cen * 1.8 + 32

}
