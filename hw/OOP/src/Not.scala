

class Not(val operand1: Expression) extends Expression {

  def execute = {
    val value1 = operand1.execute
    if (!value1.isInstanceOf[Boole])
      throw new Exception("type mismatch: only booleans can be computed")
    else {
      val num1 = value1.asInstanceOf[Boole]
      if(num1.value) Boole.FALSE else Boole.TRUE
    }
  }

  override def toString = "(" + "! "+ operand1.toString  + ")"
}

object Not {
  def apply(operand1: Expression) =
    new Not(operand1)
}
