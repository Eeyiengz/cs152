

class Description(val desc: String,
                  val price: Double,
                  val supplier: String) {
}

object Description {
  def apply(desc: String, price: Double, supplier: String): Description = new Description(desc, price, supplier)

}
