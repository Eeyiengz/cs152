

class Knight(val name: String) {
  var health = 100

  def isDead = health <= 0

  def rip = if (isDead) println(name + " is dead.")

  def attack(victim: Dragon) = {
    if (!isDead) {
      println(name + " is stabbing " + victim.name + ".")
      victim.health -= (Dungeon.random.nextInt(health) + 1)
    } else println(name + " is already dead.")
  }
}
