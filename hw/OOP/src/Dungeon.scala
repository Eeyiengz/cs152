

object Dungeon {
  val random = new scala.util.Random(System.nanoTime())

  def main(args: Array[String]): Unit = {

    val dragon = new Dragon("Dino")
    val knight = new Knight("Todd")

    while (!dragon.isDead && !knight.isDead) {
      if (!knight.isDead) knight.attack(dragon)
      if (!dragon.isDead) dragon.attack(knight)
    }
    dragon.rip
    knight.rip
  }
}