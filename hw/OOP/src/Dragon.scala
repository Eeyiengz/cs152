

class Dragon(val name: String) {
  var health = 100

  def isDead = health <= 0

  def rip = if (isDead) println(name + " is dead.")

  def attack(victim: Knight) = {
    if (!isDead) {
      println(name + " is flaming " + victim.name + ".")
      victim.health -= (Dungeon.random.nextInt(health) + 1)
    } else println(name + " is already dead.")
  }
}
