

class Item(desc: Description) {
  private val id = Item.nextId

  def getId = id

  override def toString: String = s"${desc.desc} | ${desc.price} | ${desc.supplier}"
}

object Item {
  private var id = 0

  def apply(desc: Description) = new Item(desc)

  def nextId = {
    id += 1
    id
  }
}
