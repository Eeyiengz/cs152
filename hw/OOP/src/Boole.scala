

class Boole(val value: Boolean) extends Literal {
  def &&(other: Boole): Boole = Boole(other.value && value)
  def ||(other: Boole): Boole = Boole(other.value || value)
  def <(other: Boole): Boole = Boole(other.value < value)
  def !=(other: Boole): Boole = Boole(other.value != value)
  def ==(other: Boole): Boole = Boole(other.value == value)
  override def toString  = value.toString

}

object Boole {
  val FALSE = Boole(false)
  val TRUE = Boole(true)
  def apply(value: Boolean) = new Boole(value)
}
