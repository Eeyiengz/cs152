

object TheWeatherChannel extends App {
  val station = new FarTherm

  def average = {
    var cities: List[String] = List.empty
    for (i <- 1 to CenTherm.random.nextInt(20) + 10) cities ::= ("City" + i.toString)
    station.getMeanTemperature(cities)
  }

  for (i <- 1 to 10) println("Run " + f"#$i%02d $average%2.1f degrees fahrenheit")
}
