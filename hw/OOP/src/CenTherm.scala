

class CenTherm {
  // = degrees Centigrade
  def computeTemp(city: String) = {
    // -10 to 35 degrees
    CenTherm.random.nextInt(56) - 10
  }
}

object CenTherm {
  val random = new scala.util.Random(System.nanoTime())
}