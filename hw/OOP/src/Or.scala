

class Or(val operand1: Expression, val operand2: Expression) extends Expression {

  def execute = {
    val value1 = operand1.execute
    val value2 = operand2.execute
    if (!value1.isInstanceOf[Boole] || !value2.isInstanceOf[Boole])
      throw new Exception("type mismatch: only booleans can be computed")
    else {
      val num1 = value1.asInstanceOf[Boole]
      val num2 = value2.asInstanceOf[Boole]
      num1 || num2
    }

  }

  override def toString = "(" + operand1.toString + " || " + operand2.toString + ")"
}

object Or {
  def apply(operand1: Expression, operand2: Expression) =
    new Or(operand1, operand2)
}
