object Calculator {

  // = 1 * 2 * 3 * ... * n
  def fact(n: Integer) = {
    // place iterative solution here
    var product = 1
    for (i <- 1 to n) {
      product *= i
    }
    product
  }

  // = 1 + 2 + 3 + ... + n
  def tri(n: Integer) = {
    // place iterative solution here
    var sum = 0
    for (i <- 0 to n) {
      sum += i
    }
    sum
  }

  // = 2^n
  def exp(n: Integer) = {
    // place iterative solution here
    var exp = 1
    for (i <- 1 to n) {
      exp *= 2
    }
    exp
  }

  // = true if n >= 2 and has no smaller divisors
  def isPrime(n: Integer) = {
    // place iterative solution here
    if (n < 2) false
    else if (n == 2) true
    else {
        val nBin = Integer.toBinaryString(n)
      if (nBin.last == '0') false
      else {
        var prime = true
        for (i <- 2 until n; if prime) {
          if (n % i == 0) prime = false
        }
        prime
      }
    }
  }

  def main(args: Array[String]): Unit = {
    println("enter 3 integers x, y, and z on separate lines: ")
    var x = readInt()
    var y = readInt()
    var z = readInt()
    println("fact(x) = " + fact(x))
    println("fact(y) = " + fact(y))
    println("fact(z) = " + fact(z))
    println("tri(x) = " + tri(x))
    println("tri(y) = " + tri(y))
    println("tri(z) = " + tri(z))
    println("exp(x) = " + exp(x))
    println("exp(y) = " + exp(y))
    println("exp(z) = " + exp(z))
    println("isPrime(x) = " + isPrime(x))
    println("isPrime(y) = " + isPrime(y))
    println("isPrime(z) = " + isPrime(z))
  }

}
