import Array._

object MatrixCalculator {

  // converts matrix to a string
  def toString(matrix: Array[Array[Int]]) = {
    // place code here
    var str = "\n"
    for (r <- matrix) {
      str += "\n"
      for (c <- r) {
        str += " " + c
      }
    }
    str += "\n"
    str
  }

  // returns the sum of the diagonal entries of a matrix
  def trace(m: Array[Array[Int]]) = {
    // place code here
    var result = 0
    for (diag <- m.indices) {
      result += m(diag)(diag)
    }
    result
  }

  // returns a dim x dim matrix with i/j entry = 3 * i + 2 * j % cap
  def makeArray(dim: Int, cap: Int = 100) = {
    // place code here
    var m = ofDim[Int](dim, dim)
    for (i <- m.indices; j <- m.indices) {
      m(i)(j) = 3 * i + 2 * j % cap
    }
    m
  }

  def main(args: Array[String]): Unit = {
    print("Enter a positive integer: ")
    var n = readInt
    var m = makeArray(n)
    println(toString(m))
    println("trace = " + trace(m))
  }
}
