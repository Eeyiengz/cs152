object ShapeMaker {

  def makeRectangle(rows: Int, cols: Int) = {
    // place implementation here
    var rect = ""
    for (r <- 1 to rows) {
      for (c <- 1 to cols) rect += "*"
      rect += "\n"
    }
    rect
  }

  def makeRightTriangle(rows: Int) = {
    // place implementation here
    var rTri = ""
    for (r <- 1 to rows) {
      for (c <- 1 to r) rTri += "*"
      rTri += "\n"
    }
    rTri
  }

  def makeIsoTriangle(rows: Int) = {
    // place implementation here
    val mid = rows / 2
    var iTri = ""
    for (r <- 0 to mid) {
      for (s <- 0 to mid - r) iTri += " "
      for (c <- 0 to r * 2) iTri += "*"
      iTri += "\n"
    }
    iTri
  }

  def makeInvertedTriangle(rows: Int) = {
    // place implementation here
    val mid = rows / 2
    var inTri = ""
    for (r <- 0 to mid) {
      for (s <- 0 to r) inTri += " "
      for (c <- 0 to (mid - r) * 2 ) inTri += "*"
      inTri += "\n"
    }
    inTri
  }

  def main(args: Array[String]): Unit = {
    print("Enter a positive integer: ")
    var n = readInt
    println(makeRectangle(n, n))
    println(makeRightTriangle(n))
    println(makeIsoTriangle(n))
    println(makeInvertedTriangle(n))
  }

}
