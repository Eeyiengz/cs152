object stringSession {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(131); 
  //+++++++++++++++++++
  // Problem 1
  //+++++++++++++++++++
  def isPal(str: String) = str == str.reverse;System.out.println("""isPal: (str: String)Boolean""");$skip(12); val res$0 = 
  isPal("");System.out.println("""res0: Boolean = """ + $show(res$0));$skip(13); val res$1 = 
  isPal(" ");System.out.println("""res1: Boolean = """ + $show(res$1));$skip(13); val res$2 = 
  isPal("a");System.out.println("""res2: Boolean = """ + $show(res$2));$skip(19); val res$3 = 
  isPal("rotator");System.out.println("""res3: Boolean = """ + $show(res$3));$skip(18); val res$4 = 
  isPal("123321");System.out.println("""res4: Boolean = """ + $show(res$4));$skip(18); val res$5 = 
  isPal("%#&&#%");System.out.println("""res5: Boolean = """ + $show(res$5));$skip(23); val res$6 = 
  isPal("cats & dogs");System.out.println("""res6: Boolean = """ + $show(res$6));$skip(26); val res$7 = 
  isPal("shell cordovan");System.out.println("""res7: Boolean = """ + $show(res$7));$skip(168); 
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 2
  //+++++++++++++++++++
  def isPal2(str: String) = isPal(str.replaceAll("[^a-zA-Z]", "").toLowerCase);System.out.println("""isPal2: (str: String)Boolean""");$skip(23); val res$8 = 
  isPal2(". . . . . ");System.out.println("""res8: Boolean = """ + $show(res$8));$skip(17); val res$9 = 
  isPal2(" !!!");System.out.println("""res9: Boolean = """ + $show(res$9));$skip(45); val res$10 = 
  isPal2("            a                   ");System.out.println("""res10: Boolean = """ + $show(res$10));$skip(27); val res$11 = 
  isPal2("r?o?t?a?t?o?r?");System.out.println("""res11: Boolean = """ + $show(res$11));$skip(21); val res$12 = 
  isPal2("123  321");System.out.println("""res12: Boolean = """ + $show(res$12));$skip(20); val res$13 = 
  isPal2("%#& &#%");System.out.println("""res13: Boolean = """ + $show(res$13));$skip(23); val res$14 = 
  isPal2("Don't nod.");System.out.println("""res14: Boolean = """ + $show(res$14));$skip(34); val res$15 = 
  // true
  isPal2("cats & dogs");System.out.println("""res15: Boolean = """ + $show(res$15));$skip(27); val res$16 = 
  isPal2("shell cordovan");System.out.println("""res16: Boolean = """ + $show(res$16));$skip(29); val res$17 = 
  isPal2("lkajf.e.fae lfef");System.out.println("""res17: Boolean = """ + $show(res$17));$skip(134); 
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 3
  //+++++++++++++++++++
  def mkPal(str: String) = str + str.reverse;System.out.println("""mkPal: (str: String)String""");$skip(22); val res$18 = 

  mkPal("giggity ");System.out.println("""res18: String = """ + $show(res$18));$skip(23); val res$19 = 
  mkPal("nonononoyes");System.out.println("""res19: String = """ + $show(res$19));$skip(17); val res$20 = 
  mkPal("rediv");System.out.println("""res20: String = """ + $show(res$20));$skip(15); val res$21 = 
  mkPal("red")
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 4
  //+++++++++++++++++++
  import scala.util.Random;System.out.println("""res21: String = """ + $show(res$21));$skip(307); 

  def mkWord(length: Int = Random.nextInt(10) + 1) = {
    def randChar = (Random.nextInt(26) + 0x61).toChar

    var word = ""
    for (i <- 0 until length) word += randChar
    word
  };System.out.println("""mkWord: (length: Int)String""");$skip(12); val res$22 = 
  mkWord(0);System.out.println("""res22: String = """ + $show(res$22));$skip(12); val res$23 = 
  mkWord(1);System.out.println("""res23: String = """ + $show(res$23));$skip(11); val res$24 = 
  mkWord();System.out.println("""res24: String = """ + $show(res$24));$skip(34); val res$25 = 
  // 1 to 10 letters
  mkWord(10);System.out.println("""res25: String = """ + $show(res$25));$skip(14); val res$26 = 
  mkWord(100);System.out.println("""res26: String = """ + $show(res$26));$skip(361); 

  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 5
  //+++++++++++++++++++
  def mkSentence(words: Int = Random.nextInt(11) + 5) = {
    var sentence = ""
    if (words == 0) sentence
    else {
      sentence = mkWord().capitalize
      for (i <- 1 until words) sentence = sentence + " " + mkWord()
      sentence += "."
    }
    sentence
  };System.out.println("""mkSentence: (words: Int)String""");$skip(44); val res$27 = 
  // 5 to 15 words sentences
  mkSentence();System.out.println("""res27: String = """ + $show(res$27));$skip(15); val res$28 = 
  mkSentence();System.out.println("""res28: String = """ + $show(res$28));$skip(15); val res$29 = 
  mkSentence();System.out.println("""res29: String = """ + $show(res$29));$skip(15); val res$30 = 
  mkSentence();System.out.println("""res30: String = """ + $show(res$30));$skip(15); val res$31 = 
  mkSentence();System.out.println("""res31: String = """ + $show(res$31));$skip(15); val res$32 = 
  mkSentence();System.out.println("""res32: String = """ + $show(res$32))}

  //+++++++++++++++++++
}
