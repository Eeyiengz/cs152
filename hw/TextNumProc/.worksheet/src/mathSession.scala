import scala.math._

//++++++++++++++++++++
// Problem 1
// +++++++++++++++++++
object mathSession {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(452); 
  def quadratic(a: Double, b: Double, c: Double): Option[(Double, Double)] = {
    def eq(signed: Boolean) = {
      if (signed) (-1 * b - sqrt(b * b + (-4) * a * c)) / (2 * a)
      else (-1 * b + sqrt(b * b + (-4) * a * c)) / (2 * a)
    }

    if (a == 0) None
    else if (b * b - 1 * (4 * a * c) < 0) None
    else Some(eq(false), eq(true))
  };System.out.println("""quadratic: (a: Double, b: Double, c: Double)Option[(Double, Double)]""");$skip(37); val res$0 = 

  // complex
  quadratic(2, -4, 7);System.out.println("""res0: Option[(Double, Double)] = """ + $show(res$0));$skip(23); val res$1 = 
  quadratic(1, -3, 10);System.out.println("""res1: Option[(Double, Double)] = """ + $show(res$1));$skip(34); val res$2 = 
  // 1 root
  quadratic(1, -2, 1);System.out.println("""res2: Option[(Double, Double)] = """ + $show(res$2));$skip(21); val res$3 = 
  quadratic(1, 0, 0);System.out.println("""res3: Option[(Double, Double)] = """ + $show(res$3));$skip(35); val res$4 = 
  // 2 roots
  quadratic(-1, 1, 1);System.out.println("""res4: Option[(Double, Double)] = """ + $show(res$4));$skip(22); val res$5 = 
  quadratic(1, 0, -1);System.out.println("""res5: Option[(Double, Double)] = """ + $show(res$5));$skip(222); 
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 2
  //+++++++++++++++++++

  def distance(a: (Double, Double), b: (Double, Double)) =
    sqrt((b._1 - a._1) * (b._1 - a._1) + (b._2 - a._2) * (b._2 - a._2));System.out.println("""distance: (a: (Double, Double), b: (Double, Double))Double""");$skip(29); val res$6 = 

  distance((1, 1), (0, 0));System.out.println("""res6: Double = """ + $show(res$6));$skip(27); val res$7 = 
  distance((3, 0), (0, 0));System.out.println("""res7: Double = """ + $show(res$7));$skip(27); val res$8 = 
  distance((1, 1), (1, 1));System.out.println("""res8: Double = """ + $show(res$8));$skip(34); val res$9 = 
  // 0
  distance((1, 0), (1, 5));System.out.println("""res9: Double = """ + $show(res$9));$skip(212); 
  // 5
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 3
  //+++++++++++++++++++

  def dot(a: (Double, Double, Double), b: (Double, Double, Double)) =
    a._1 * b._1 + a._2 * b._2 + a._3 * b._3;System.out.println("""dot: (a: (Double, Double, Double), b: (Double, Double, Double))Double""");$skip(34); val res$10 = 

  dot((2.0, 3, 4), (2, 2.0, 2));System.out.println("""res10: Double = """ + $show(res$10));$skip(28); val res$11 = 
  dot((1, 1, 1), (1, 1, 1));System.out.println("""res11: Double = """ + $show(res$11));$skip(35); val res$12 = 
  // 3
  dot((2, 2, 2), (2, 2, 2));System.out.println("""res12: Double = """ + $show(res$12));$skip(36); val res$13 = 
  // 12
  dot((1, 0, 1), (0, 1, 0));System.out.println("""res13: Double = """ + $show(res$13));$skip(418); 
  // 0
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 6
  //+++++++++++++++++++
  def isPrime(n: Int) = {
    if (n < 2) false
    else if (n == 2) true
    else {
      val nBin = Integer.toBinaryString(n)
      if (nBin.last == '0') false
      else {
        var prime = true
        for (i <- 2 until n; if prime) {
          if (n % i == 0) prime = false
        }
        prime
      }
    }
  };System.out.println("""isPrime: (n: Int)Boolean""");$skip(13); val res$14 = 
  isPrime(0);System.out.println("""res14: Boolean = """ + $show(res$14));$skip(13); val res$15 = 
  isPrime(1);System.out.println("""res15: Boolean = """ + $show(res$15));$skip(13); val res$16 = 
  isPrime(2);System.out.println("""res16: Boolean = """ + $show(res$16));$skip(13); val res$17 = 
  isPrime(8);System.out.println("""res17: Boolean = """ + $show(res$17));$skip(266); 
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 7
  //+++++++++++++++++++

  def phi(n: Int) = {
    var count = 0
    def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
    for (i <- 1 to n) if (gcd(n, i) == 1) count += 1
    count
  };System.out.println("""phi: (n: Int)Int""");$skip(9); val res$18 = 
  phi(9);System.out.println("""res18: Int = """ + $show(res$18));$skip(10); val res$19 = 
  phi(10);System.out.println("""res19: Int = """ + $show(res$19));$skip(10); val res$20 = 
  phi(45);System.out.println("""res20: Int = """ + $show(res$20));$skip(18); val res$21 = 
  // 24
  phi(99);System.out.println("""res21: Int = """ + $show(res$21));$skip(153); 
  // 60
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 8
  //+++++++++++++++++++

  def rollDice = (ceil(random * 6), ceil(random * 6));System.out.println("""rollDice: => (Double, Double)""");$skip(11); val res$22 = 
  rollDice;System.out.println("""res22: (Double, Double) = """ + $show(res$22));$skip(11); val res$23 = 
  rollDice;System.out.println("""res23: (Double, Double) = """ + $show(res$23));$skip(11); val res$24 = 
  rollDice;System.out.println("""res24: (Double, Double) = """ + $show(res$24))}
  //+++++++++++++++++++
}
