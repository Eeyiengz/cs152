import scala.io.StdIn._

object console {
  def repl {
    println("Welcome to the Console. Type help for commands.")
    var in = ""
    do {
      print("-> ")
      in = readLine.toLowerCase.trim
      val tokens = in.split("\\s")
      try {
        tokens(0) match {
          case "help" => println("commands: add, mul, sub, div, quit, help")
          case "add" | "mul" | "sub" | "div" => try {
            if (tokens.length != 3) throw new Exception("Insufficient input.")
            else if (tokens(0) == "add") println(add(tokens(1).toDouble, tokens(2).toDouble))
            else if (tokens(0) == "sub") println(sub(tokens(1).toDouble, tokens(2).toDouble))
            else if (tokens(0) == "mul") println(mul(tokens(1).toDouble, tokens(2).toDouble))
            else if (tokens(0) == "div") println(div(tokens(1).toDouble, tokens(2).toDouble))
            else {
              // nothing }
            }
          } catch {
            case e: Throwable => println(e.getMessage)
          }
          case "quit" => // no action
          case _ => println("No actions taken. Unrecognized command: " + tokens(0))
        }
      } catch {
        case e: Throwable => println("Bad input.")
      }
    } while (in != "quit")
  }

  def main(args: Array[String]): Unit = {
    repl
  }

  def add(a: Double, b: Double) = a + b

  def mul(a: Double, b: Double) = a * b

  def sub(a: Double, b: Double) = a - b

  def div(a: Double, b: Double) = a / b
}
