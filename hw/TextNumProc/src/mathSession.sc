import scala.math._

//++++++++++++++++++++
// Problem 1
// +++++++++++++++++++
object mathSession {
  def quadratic(a: Double, b: Double, c: Double): Option[(Double, Double)] = {
    def eq(signed: Boolean) = {
      if (signed) (-1 * b - sqrt(b * b + (-4) * a * c)) / (2 * a)
      else (-1 * b + sqrt(b * b + (-4) * a * c)) / (2 * a)
    }

    if (a == 0) None
    else if (b * b - 1 * (4 * a * c) < 0) None
    else Some(eq(false), eq(true))
  }                                               //> quadratic: (a: Double, b: Double, c: Double)Option[(Double, Double)]

  // complex
  quadratic(2, -4, 7)                             //> res0: Option[(Double, Double)] = None
  quadratic(1, -3, 10)                            //> res1: Option[(Double, Double)] = None
  // 1 root
  quadratic(1, -2, 1)                             //> res2: Option[(Double, Double)] = Some((1.0,1.0))
  quadratic(1, 0, 0)                              //> res3: Option[(Double, Double)] = Some((0.0,-0.0))
  // 2 roots
  quadratic(-1, 1, 1)                             //> res4: Option[(Double, Double)] = Some((-0.6180339887498949,1.618033988749895
                                                  //| ))
  quadratic(1, 0, -1)                             //> res5: Option[(Double, Double)] = Some((1.0,-1.0))
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 2
  //+++++++++++++++++++

  def distance(a: (Double, Double), b: (Double, Double)) =
    sqrt((b._1 - a._1) * (b._1 - a._1) + (b._2 - a._2) * (b._2 - a._2))
                                                  //> distance: (a: (Double, Double), b: (Double, Double))Double

  distance((1, 1), (0, 0))                        //> res6: Double = 1.4142135623730951
  distance((3, 0), (0, 0))                        //> res7: Double = 3.0
  distance((1, 1), (1, 1))                        //> res8: Double = 0.0
  // 0
  distance((1, 0), (1, 5))                        //> res9: Double = 5.0
  // 5
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 3
  //+++++++++++++++++++

  def dot(a: (Double, Double, Double), b: (Double, Double, Double)) =
    a._1 * b._1 + a._2 * b._2 + a._3 * b._3       //> dot: (a: (Double, Double, Double), b: (Double, Double, Double))Double

  dot((2.0, 3, 4), (2, 2.0, 2))                   //> res10: Double = 18.0
  dot((1, 1, 1), (1, 1, 1))                       //> res11: Double = 3.0
  // 3
  dot((2, 2, 2), (2, 2, 2))                       //> res12: Double = 12.0
  // 12
  dot((1, 0, 1), (0, 1, 0))                       //> res13: Double = 0.0
  // 0
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 6
  //+++++++++++++++++++
  def isPrime(n: Int) = {
    if (n < 2) false
    else if (n == 2) true
    else {
      val nBin = Integer.toBinaryString(n)
      if (nBin.last == '0') false
      else {
        var prime = true
        for (i <- 2 until n; if prime) {
          if (n % i == 0) prime = false
        }
        prime
      }
    }
  }                                               //> isPrime: (n: Int)Boolean
  isPrime(0)                                      //> res14: Boolean = false
  isPrime(1)                                      //> res15: Boolean = false
  isPrime(2)                                      //> res16: Boolean = true
  isPrime(8)                                      //> res17: Boolean = false
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 7
  //+++++++++++++++++++

  def phi(n: Int) = {
    var count = 0
    def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
    for (i <- 1 to n) if (gcd(n, i) == 1) count += 1
    count
  }                                               //> phi: (n: Int)Int
  phi(9)                                          //> res18: Int = 6
  phi(10)                                         //> res19: Int = 4
  phi(45)                                         //> res20: Int = 24
  // 24
  phi(99)                                         //> res21: Int = 60
  // 60
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 8
  //+++++++++++++++++++

  def rollDice = (ceil(random * 6), ceil(random * 6))
                                                  //> rollDice: => (Double, Double)
  rollDice                                        //> res22: (Double, Double) = (6.0,6.0)
  rollDice                                        //> res23: (Double, Double) = (2.0,2.0)
  rollDice                                        //> res24: (Double, Double) = (1.0,1.0)
  //+++++++++++++++++++
}