object stringSession {
  //+++++++++++++++++++
  // Problem 1
  //+++++++++++++++++++
  def isPal(str: String) = str == str.reverse     //> isPal: (str: String)Boolean
  isPal("")                                       //> res0: Boolean = true
  isPal(" ")                                      //> res1: Boolean = true
  isPal("a")                                      //> res2: Boolean = true
  isPal("rotator")                                //> res3: Boolean = true
  isPal("123321")                                 //> res4: Boolean = true
  isPal("%#&&#%")                                 //> res5: Boolean = true
  isPal("cats & dogs")                            //> res6: Boolean = false
  isPal("shell cordovan")                         //> res7: Boolean = false
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 2
  //+++++++++++++++++++
  def isPal2(str: String) = isPal(str.replaceAll("[^a-zA-Z]", "").toLowerCase)
                                                  //> isPal2: (str: String)Boolean
  isPal2(". . . . . ")                            //> res8: Boolean = true
  isPal2(" !!!")                                  //> res9: Boolean = true
  isPal2("            a                   ")      //> res10: Boolean = true
  isPal2("r?o?t?a?t?o?r?")                        //> res11: Boolean = true
  isPal2("123  321")                              //> res12: Boolean = true
  isPal2("%#& &#%")                               //> res13: Boolean = true
  isPal2("Don't nod.")                            //> res14: Boolean = true
  // true
  isPal2("cats & dogs")                           //> res15: Boolean = false
  isPal2("shell cordovan")                        //> res16: Boolean = false
  isPal2("lkajf.e.fae lfef")                      //> res17: Boolean = false
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 3
  //+++++++++++++++++++
  def mkPal(str: String) = str + str.reverse      //> mkPal: (str: String)String

  mkPal("giggity ")                               //> res18: String = giggity  ytiggig
  mkPal("nonononoyes")                            //> res19: String = nonononoyesseyonononon
  mkPal("rediv")                                  //> res20: String = redivvider
  mkPal("red")                                    //> res21: String = redder
  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 4
  //+++++++++++++++++++
  import scala.util.Random

  def mkWord(length: Int = Random.nextInt(10) + 1) = {
    def randChar = (Random.nextInt(26) + 0x61).toChar

    var word = ""
    for (i <- 0 until length) word += randChar
    word
  }                                               //> mkWord: (length: Int)String
  mkWord(0)                                       //> res22: String = ""
  mkWord(1)                                       //> res23: String = c
  mkWord()                                        //> res24: String = nicuetcc
  // 1 to 10 letters
  mkWord(10)                                      //> res25: String = mspcdqlmbp
  mkWord(100)                                     //> res26: String = hhnldotchhchcjwvjausnjfjerpmatigthspyjxuytgkaomvzamvwmrsqli
                                                  //| pradjdxeuucvsxnxqgwjvoatwdjhqociyldpjngfn

  //+++++++++++++++++++

  //+++++++++++++++++++
  // Problem 5
  //+++++++++++++++++++
  def mkSentence(words: Int = Random.nextInt(11) + 5) = {
    var sentence = ""
    if (words == 0) sentence
    else {
      sentence = mkWord().capitalize
      for (i <- 1 until words) sentence = sentence + " " + mkWord()
      sentence += "."
    }
    sentence
  }                                               //> mkSentence: (words: Int)String
  // 5 to 15 words sentences
  mkSentence()                                    //> res27: String = O xod glsg unqsxekvd nvwwyavywl dzkblogaz ppnudto fmfqb goh
                                                  //| angpivw.
  mkSentence()                                    //> res28: String = T xstvhqe uloiz daokjy fybpl.
  mkSentence()                                    //> res29: String = Mdcbnl eye imemlgjhvd kbgpld xccvtjhr xiupj q qowfp yl pwrn
                                                  //| vjee k.
  mkSentence()                                    //> res30: String = Pzmb h mrajwhyei fepp yid jokibtcjd auhpe rpdlfgg fdd xhwgu
                                                  //| duh.
  mkSentence()                                    //> res31: String = Xwanyedu qxbdxp ibmmoment opet q ttosfm kulshptond qdl dmsl
                                                  //| sthrah cxgyrqmtkh otcuyjis xm ddzcroul zogd uitsxaye.
  mkSentence()                                    //> res32: String = Bpuflfcy oam ceheczbbzv ysidy l ycg gcgyecgtbi tzktfjub zmc
                                                  //| fjanqp dhvgwk umxedgvx.

  //+++++++++++++++++++
}