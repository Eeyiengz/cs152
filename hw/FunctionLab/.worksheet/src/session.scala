object session {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(60); 
  println("Welcome to the Scala worksheet");$skip(149); 

  // problem 1 _________________________________________
  def compose[T](f: T => T, g: T => T): T => T = {
    def r(x: T): T = f(g(x))

    r
  };System.out.println("""compose: [T](f: T => T, g: T => T)T => T""");$skip(43); 

  // test p1
  def triple(x: Int) = 3 * x;System.out.println("""triple: (x: Int)Int""");$skip(30); 

  def square(x: Int) = x * x;System.out.println("""square: (x: Int)Int""");$skip(46); 

  val squareTriple = compose(square, triple);System.out.println("""squareTriple  : Int => Int = """ + $show(squareTriple ));$skip(18); val res$0 = 
  squareTriple(5);System.out.println("""res0: Int = """ + $show(res$0));$skip(379); 

  // problem 2 _________________________________________
  def selfIter[T](f: T => T, n: Int): T => T = {
    def rep: T => T = {
      def helper(result: T => T, count: Int = 1): T => T =
        if (count > n) result
        else helper(compose(f, result), count + 1)

      helper(f, 1)
    }

    if (n == 0) {
      def id: T => T = T => T

      id
    }
    else rep
  };System.out.println("""selfIter: [T](f: T => T, n: Int)T => T""");$skip(43); 

  // test p2
  def inc(x: Double) = x + 1;System.out.println("""inc: (x: Double)Double""");$skip(33); 

  def double(x: Double) = 2 * x;System.out.println("""double: (x: Double)Double""");$skip(23); val res$1 = 

  selfIter(inc, 0)(0);System.out.println("""res1: Double = """ + $show(res$1));$skip(22); val res$2 = 
  selfIter(inc, 0)(1);System.out.println("""res2: Double = """ + $show(res$2));$skip(22); val res$3 = 
  selfIter(inc, 1)(0);System.out.println("""res3: Double = """ + $show(res$3));$skip(23); val res$4 = 
  selfIter(inc, 20)(0);System.out.println("""res4: Double = """ + $show(res$4));$skip(25); val res$5 = 
  selfIter(double, 0)(5);System.out.println("""res5: Double = """ + $show(res$5));$skip(25); val res$6 = 
  selfIter(double, 1)(5);System.out.println("""res6: Double = """ + $show(res$6));$skip(26); val res$7 = 
  selfIter(double, 20)(1);System.out.println("""res7: Double = """ + $show(res$7));$skip(325); 


  // problem 3 _________________________________________
  def countPass[T](arr: Array[T]): Int = {
    def helper(count: Int, result: Int): Int =
      if (count == arr.length) result
      else if (arr(count).isInstanceOf[Boolean]) helper(count + 1, result + 1)
      else helper(count + 1, result)

    helper(0, 0)
  };System.out.println("""countPass: [T](arr: Array[T])Int""");$skip(114); val res$8 = 

  // test p3
  // array of 5 Boolean elements
  countPass(true :: true :: false :: false :: true :: Nil toArray);System.out.println("""res8: Int = """ + $show(res$8));$skip(87); val res$9 = 
  // 1 instance of Boolean
  countPass(true :: "should" :: "be" :: "1" :: Nil toArray);System.out.println("""res9: Int = """ + $show(res$9));$skip(336); 


  // problem 5 _________________________________________
  def controlLoop[S](state: S, cycle: Int,
                     halt: (S, Int) => Boolean,
                     update: (S, Int) => S): S = {
    def helper(s: S, c: Int): S = {
      if (halt(s, c)) s
      else helper(update(s, c), c + 1)
    }

    helper(state, cycle)
  }


  // problem 6 _________________________________________
  object amoeba {
    val initPop = 1
    val finPop = 100000

    val cycle = 0

    // week
    def halt(pop: Int, cycle: Int) = pop > finPop

    def update(pop: Int, cycle: Int) = pop * 2
  };System.out.println("""controlLoop: [S](state: S, cycle: Int, halt: (S, Int) => Boolean, update: (S, Int) => S)S""");$skip(345); val res$10 = 

  // grow amoeba
  controlLoop(amoeba.initPop, amoeba.cycle, amoeba.halt, amoeba.update);System.out.println("""res10: Int = """ + $show(res$10));$skip(442); 
  // log2(131072) = 17 cycles/weeks


  // problem 7 _________________________________________
  def solve(f: Double => Double): Double = {
    val delta = 0.1
    val initX = 1.0
    val cycle = 0

    // iteration
    def halt(r: Double, cycle: Int) = Math.abs(f(r)) <= delta

    def fp(r: Double) = ((f(r + delta) - f(r)) / delta)

    def update(r: Double, cycle: Int) = r - f(r) / fp(r)

    controlLoop(initX, cycle, halt, update)
  };System.out.println("""solve: (f: Double => Double)Double""");$skip(130); 

  // problem 8 _________________________________________
  def squareRoot(x: Double) = solve((r: Double) => math.abs(r * r - x));System.out.println("""squareRoot: (x: Double)Double""");$skip(17); val res$11 = 

  squareRoot(1);System.out.println("""res11: Double = """ + $show(res$11));$skip(16); val res$12 = 
  squareRoot(4);System.out.println("""res12: Double = """ + $show(res$12));$skip(17); val res$13 = 
  squareRoot(64);System.out.println("""res13: Double = """ + $show(res$13));$skip(22); val res$14 = 
  squareRoot(1234568);System.out.println("""res14: Double = """ + $show(res$14));$skip(133); 


  // problem 9 _________________________________________
  def cubeRoot(x: Double) = solve((r: Double) => math.abs(r * r * r - x));System.out.println("""cubeRoot: (x: Double)Double""");$skip(16); val res$15 = 

  cubeRoot(27);System.out.println("""res15: Double = """ + $show(res$15));$skip(16); val res$16 = 
  cubeRoot(729);System.out.println("""res16: Double = """ + $show(res$16));$skip(20); val res$17 = 
  cubeRoot(2302343);System.out.println("""res17: Double = """ + $show(res$17));$skip(145); 

  // problem 10 _________________________________________
  def nthRoot(x: Double, n: Int) = solve((r: Double) => math.abs(math.pow(r, n) - x));System.out.println("""nthRoot: (x: Double, n: Int)Double""");$skip(19); val res$18 = 

  nthRoot(729, 3);System.out.println("""res18: Double = """ + $show(res$18))}
  // == cubeRoot(729)
  
}
