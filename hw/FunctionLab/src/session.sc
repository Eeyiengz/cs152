object session {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  // problem 1 _________________________________________
  def compose[T](f: T => T, g: T => T): T => T = {
    def r(x: T): T = f(g(x))

    r
  }                                               //> compose: [T](f: T => T, g: T => T)T => T

  // test p1
  def triple(x: Int) = 3 * x                      //> triple: (x: Int)Int

  def square(x: Int) = x * x                      //> square: (x: Int)Int

  val squareTriple = compose(square, triple)      //> squareTriple  : Int => Int = <function1>
  squareTriple(5)                                 //> res0: Int = 225

  // problem 2 _________________________________________
  def selfIter[T](f: T => T, n: Int): T => T = {
    def rep: T => T = {
      def helper(result: T => T, count: Int = 1): T => T =
        if (count > n) result
        else helper(compose(f, result), count + 1)

      helper(f, 1)
    }

    if (n == 0) {
      def id: T => T = T => T

      id
    }
    else rep
  }                                               //> selfIter: [T](f: T => T, n: Int)T => T

  // test p2
  def inc(x: Double) = x + 1                      //> inc: (x: Double)Double

  def double(x: Double) = 2 * x                   //> double: (x: Double)Double

  selfIter(inc, 0)(0)                             //> res1: Double = 0.0
  selfIter(inc, 0)(1)                             //> res2: Double = 1.0
  selfIter(inc, 1)(0)                             //> res3: Double = 2.0
  selfIter(inc, 20)(0)                            //> res4: Double = 21.0
  selfIter(double, 0)(5)                          //> res5: Double = 5.0
  selfIter(double, 1)(5)                          //> res6: Double = 20.0
  selfIter(double, 20)(1)                         //> res7: Double = 2097152.0


  // problem 3 _________________________________________
  def countPass[T](arr: Array[T]): Int = {
    def helper(count: Int, result: Int): Int =
      if (count == arr.length) result
      else if (arr(count).isInstanceOf[Boolean]) helper(count + 1, result + 1)
      else helper(count + 1, result)

    helper(0, 0)
  }                                               //> countPass: [T](arr: Array[T])Int

  // test p3
  // array of 5 Boolean elements
  countPass(true :: true :: false :: false :: true :: Nil toArray)
                                                  //> res8: Int = 5
  // 1 instance of Boolean
  countPass(true :: "should" :: "be" :: "1" :: Nil toArray)
                                                  //> res9: Int = 1


  // problem 5 _________________________________________
  def controlLoop[S](state: S, cycle: Int,
                     halt: (S, Int) => Boolean,
                     update: (S, Int) => S): S = {
    def helper(s: S, c: Int): S = {
      if (halt(s, c)) s
      else helper(update(s, c), c + 1)
    }

    helper(state, cycle)
  }                                               //> controlLoop: [S](state: S, cycle: Int, halt: (S, Int) => Boolean, update: (
                                                  //| S, Int) => S)S


  // problem 6 _________________________________________
  object amoeba {
    val initPop = 1
    val finPop = 100000

    val cycle = 0

    // week
    def halt(pop: Int, cycle: Int) = pop > finPop

    def update(pop: Int, cycle: Int) = pop * 2
  }

  // grow amoeba
  controlLoop(amoeba.initPop, amoeba.cycle, amoeba.halt, amoeba.update)
                                                  //> res10: Int = 131072
  // log2(131072) = 17 cycles/weeks


  // problem 7 _________________________________________
  def solve(f: Double => Double): Double = {
    val delta = 0.1
    val initX = 1.0
    val cycle = 0

    // iteration
    def halt(r: Double, cycle: Int) = Math.abs(f(r)) <= delta

    def fp(r: Double) = ((f(r + delta) - f(r)) / delta)

    def update(r: Double, cycle: Int) = r - f(r) / fp(r)

    controlLoop(initX, cycle, halt, update)
  }                                               //> solve: (f: Double => Double)Double

  // problem 8 _________________________________________
  def squareRoot(x: Double) = solve((r: Double) => math.abs(r * r - x))
                                                  //> squareRoot: (x: Double)Double

  squareRoot(1)                                   //> res11: Double = 1.0
  squareRoot(4)                                   //> res12: Double = 2.0015885096407553
  squareRoot(64)                                  //> res13: Double = 8.005366408212588
  squareRoot(1234568)                             //> res14: Double = 1111.111155576593


  // problem 9 _________________________________________
  def cubeRoot(x: Double) = solve((r: Double) => math.abs(r * r * r - x))
                                                  //> cubeRoot: (x: Double)Double

  cubeRoot(27)                                    //> res15: Double = 3.0024354705953327
  cubeRoot(729)                                   //> res16: Double = 9.000095795972713
  cubeRoot(2302343)                               //> res17: Double = 132.0454201670468

  // problem 10 _________________________________________
  def nthRoot(x: Double, n: Int) = solve((r: Double) => math.abs(math.pow(r, n) - x))
                                                  //> nthRoot: (x: Double, n: Int)Double

  nthRoot(729, 3)                                 //> res18: Double = 9.000095795972713
  // == cubeRoot(729)
  
}