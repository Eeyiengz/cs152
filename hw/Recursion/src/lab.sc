object RecursionLab {
  def inc(n: Int) = n + 1

  def dec(n: Int) = n - 1

  def isZero(n: Int) = n == 0

  // ++++++++++++++++++++++++++
  // Problem 1
  // ++++++++++++++++++++++++++
  def add(n: Int, m: Int): Int = if (isZero(m)) n else add(inc(n), dec(m))

  add(4, 5)
  add(6, 3)
  // ++++++++++++++++++++++++++


  // ++++++++++++++++++++++++++
  // Problem 2
  // ++++++++++++++++++++++++++
  def mul(n: Int, m: Int): Int = if (m == 1) n else if (isZero(m)) 0 else add(n, mul(n, dec(m)))

  mul(2, 10)
  mul(5, 5)
  mul(3, 1)
  mul(2, 0)
  // ++++++++++++++++++++++++++


  // ++++++++++++++++++++++++++
  // Problem 3
  // ++++++++++++++++++++++++++
  def exp2(m: Int): Int = if (isZero(m)) 1 else mul(2, exp2(dec(m)))

  exp2(10)
  exp2(6)
  exp2(1)
  exp2(0)
  // ++++++++++++++++++++++++++


  // ++++++++++++++++++++++++++
  // Problem 4
  // ++++++++++++++++++++++++++
  def hyperExp(n: Int): Int = if (isZero(n)) 1 else exp2(hyperExp(dec(n)))

  hyperExp(0)
  hyperExp(1)
  hyperExp(2)
  val hT0 = System.nanoTime()
  hyperExp(3)
  val hT1 = System.nanoTime() - hT0
  // hyperExp(4) stack overflow

  // ++++++++++++++++++++++++++


  // ++++++++++++++++++++++++++
  // Problem 5.1
  // ++++++++++++++++++++++++++
  def tailAdd(n: Int, m: Int) = {
    def helper(result: Int, count: Int): Int = {
      if (count == m) result else helper(inc(result), inc(count))
    }

    helper(n, 0)
  }

  tailAdd(5, 4)
  tailAdd(3, 6)

  // ++++++++++++++++++++++++++
  // Problem 5.2
  // ++++++++++++++++++++++++++
  def tailMul(n: Int, m: Int) = {
    def helper(result: Int, count: Int): Int = {
      if (count == m) result else helper(tailAdd(result, n), inc(count))
    }

    helper(n, 1)
  }

  tailMul(5, 5)
  tailMul(3, 3)
  tailMul(25, 25)

  // ++++++++++++++++++++++++++
  // Problem 5.3
  // ++++++++++++++++++++++++++
  def tailExp2(n: Int) = {
    def helper(result: Int, count: Int): Int = {
      if (isZero(n)) 1 else if (count == n) result else helper(tailMul(result, 2), inc(count))
    }

    helper(2, 1)
  }

  tailExp2(0)
  tailExp2(1)
  tailExp2(2)
  tailExp2(3)
  tailExp2(4)
  tailExp2(5)
  tailExp2(10)
  tailExp2(20)

  // ++++++++++++++++++++++++++
  // Problem 5.4
  // ++++++++++++++++++++++++++
  def tailHyper(n: Int) = {
    def helper(result: Int, count: Int): Int = {
      if (isZero(n)) 1 else if (count == n) result else helper(tailExp2(result), inc(count))
    }

    helper(2, 1)
  }

  tailHyper(0)
  tailHyper(1)
  tailHyper(2)
  val thT0 = System.nanoTime()
  tailHyper(3)
  val thT1 = System.nanoTime() - thT0
  tailHyper(4)
  tailHyper(5)

  // ++++++++++++++++++++++++++
  // Problem 5.5
  // ++++++++++++++++++++++++++
  // Tail recursion solves the stack overflow issue and increased peformance
  // by over 2 folds as apparent by value hT1 and thT1
  // ++++++++++++++++++++++++++


  // ++++++++++++++++++++++++++
  // Problem 9
  // ++++++++++++++++++++++++++
  def tailFib(n: BigInt): BigInt = {
    def helper(previous: BigInt, result: BigInt, count: BigInt): BigInt = {
      if (n == 0) 0 else if (n == 1) 1 else if (count == n) result else helper(result, result + previous, count + 1)
    }

    helper(0, 1, 1)
  }

  tailFib(0)
  tailFib(1)
  tailFib(2)
  tailFib(3)
  tailFib(4)
  tailFib(5)
  tailFib(6)
  tailFib(7)
  tailFib(30)
  tailFib(50)
  tailFib(99)
  tailFib(2142) // no stack overflow or hang
  // ++++++++++++++++++++++++++


  // ++++++++++++++++++++++++++
  // Problem 10
  // ++++++++++++++++++++++++++
  def choose(n: BigInt, m: BigInt): BigInt = {
    def helper(result: BigInt, nCount: BigInt, mCount: BigInt, nm: BigInt): BigInt = {
      if (m > n) 0 else if (m == n) 1 else if (nCount > n && mCount > m) result else {
        if (nCount <= n) helper(result * nCount, nCount + 1, mCount, nm)
        else if (mCount <= m) helper(result / mCount, nCount, mCount + 1, nm)
        else if (nm <= n - m) helper(result / nm, nCount, mCount, nm + 1)
        else {
          result
        }
      }
    }

    helper(1, 1, 1, n - m)
  }

  choose(5, 4)
  // ++++++++++++++++++++++++++
}