public interface Expression {
    double execute();
}
