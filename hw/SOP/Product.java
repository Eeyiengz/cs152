import java.util.ArrayList;
import java.util.List;

public class Product implements Expression {
    private List<Double> operands;

    public Product() {
        operands = new ArrayList<>();
    }

    public Product(List l) {
        operands = l;
    }

    public void add(double operand) {
        operands.add(operand);
    }

    public int getCount() {
        return operands.size();
    }

    @Override
    public double execute() {
        if (getCount() == 1) {
            return operands.get(0);
        } else {
            double total = 0;
            for (double op : operands) {
                if (total == 0) total = 1.0;
                total = total * op;
            }
            return total;
        }
    }
}
