import java.util.Arrays;
import java.util.Scanner;

public class Console {
    private Scanner in = new Scanner(System.in);

    public void repl() {
        while (true) {
            try {
                System.out.print("-> ");
                String input = in.nextLine();
                System.out.println(input);
                ;
                if (input.toLowerCase().equals(":quit") || input.toLowerCase().equals(":q")) break;
                else {
                    System.out.println(parse(input).execute());
                }
            } catch (Exception e) {
                System.out.println("Error, " + e.getMessage());
            }
        }
    }

    public Expression parse(String line) {
        Expression SOP = new Sum();
        Scanner expr = new Scanner(line).useDelimiter("\\s*\\+\\s*");
        while (expr.hasNext()) {
            String SOPtoken = expr.next();
            Product p = new Product();
            try (Scanner pIn = new Scanner(SOPtoken).useDelimiter("\\s*\\*\\s*")) {
                while (pIn.hasNext()) {
                    p.add(Double.parseDouble(pIn.next()));
                }
            }
            ((Sum) SOP).add(p);
        }
        return SOP;
    }

    public static void main(String[] args) {
        Console console = new Console();
        console.repl();
    }
}
