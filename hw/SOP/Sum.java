import java.util.ArrayList;
import java.util.List;

public class Sum implements Expression {
    private List<Product> operands;

    public Sum() {
        operands = new ArrayList<>();
    }

    public void add(Product operand) {
        operands.add(operand);
    }

    @Override
    public double execute() {
        double total = 0.0;
        for (Product prod : operands) {
            total = total + prod.execute();
        }
        return total;
    }
}
