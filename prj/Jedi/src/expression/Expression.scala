package expression

import system.{JediException, TypeException, UndefinedException, alu}
import value._

trait Expression {
  def execute(env: Environment): Value
}

trait Literal extends Expression with Value {
  def execute(env: Environment): Literal = this
}

trait SpecialForm extends Expression

case class Identifier(val name: String) extends Expression {
  override def execute(env: Environment): Value = env(this)
}

case class FunCall(val operator: Identifier, val operands: List[Expression]) extends Expression {
  override def execute(env: Environment): Value = {
    val args = operands.map(_.execute(env))
    try {
      val fun = env(operator)
      if (!fun.isInstanceOf[Closure]) throw new TypeException("not a function")
      fun.asInstanceOf[Closure].apply(args)
    } catch {
      case e: UndefinedException => alu.execute(operator, args)
    }
  }
}

case class Declaration(val name: Identifier, val body: Expression) extends SpecialForm {
  override def execute(env: Environment): Value = {
    val result = body.execute(env)
    env.put(name, result)
    Notification.OK
  }
}

case class Disjunction(val exps: List[Expression]) extends SpecialForm {
  override def execute(env: Environment): Value = {
    var more = true
    var result: Value = Boole(false)
    for (exp <- exps if more) {
      val arg = exp.execute(env)
      if (!arg.isInstanceOf[Boole]) throw new TypeException(arg.toString)
      val b = arg.asInstanceOf[Boole]
      if (b.value) {
        result = Boole.TRUE
        more = false
      }
    }
    result
  }
}

case class Conjunction(val exps: List[Expression]) extends SpecialForm {
  override def execute(env: Environment): Value = {
    var more = true
    var result = Boole.TRUE
    for (exp <- exps if more) {
      val arg = exp.execute(env)
      if (!arg.isInstanceOf[Boole]) throw new TypeException(arg.toString)
      val b = arg.asInstanceOf[Boole]
      if (!b.value) {
        result = Boole.FALSE
        more = false
      }
    }
    result
  }
}

case class Conditional(condition: Expression, consequent: Expression, alternative: Expression = null) extends SpecialForm {
  def execute(env: Environment) = {
    val value = condition.execute(env)
    if (!value.isInstanceOf[Boole]) throw new TypeException(value.toString() + " must be an instance of Boole")
    if (value.asInstanceOf[Boole].value) {
      consequent.execute(env)
    }
    else if (alternative != null) alternative.execute(env)
    else Notification.UNDF
  }
}

case class Block(val locals: List[Expression]) extends SpecialForm {
  override def execute(env: Environment): Value = {
    val newEnv = new Environment(env)
    var result: Value = Notification.UNDF
    for (local <- locals) result = local.execute(newEnv)
    result
  }
}

case class Lambda(params: List[Identifier], body: Expression) extends SpecialForm {
  override def execute(env: Environment): Value = {
    new Closure(params, body, env)
  }
}

case class Iteration(cond: Expression, body: Expression) extends SpecialForm {
  override def execute(env: Environment): Value = {
    var exec: Value = Notification.UNDF
    val c = cond.execute(env)
    if (!c.isInstanceOf[Boole]) throw new TypeException("condition must be a Boole")
    var cboole = c.asInstanceOf[Boole]
    while (cboole.value) {
      exec = body.execute(env)
      cboole = cond.execute(env).asInstanceOf[Boole]
    }
    exec
  }
}

case class Assignment(iden: Identifier, exp: Expression) extends SpecialForm {
  override def execute(env: Environment): Value = {
    val a = iden.execute(env)
    if (!a.isInstanceOf[Variable]) throw new TypeException("value can't be changed")
    a.asInstanceOf[Variable].value = exp.execute(env)
    Notification.OK
  }
}

case class Switch(select: Expression, b: Expression) extends SpecialForm {
  override def execute(env: Environment): Value = {
    val sel = select.execute(env)
    val bl = b.asInstanceOf[Block]
    if (!sel.isInstanceOf[Number]) throw new TypeException("not a number type")
    val num = sel.asInstanceOf[Number]
    var blo: Value = Notification.UNDF
    if (num.value < bl.locals.length) blo = Block(List(bl.locals(num.value.toInt))).execute(env)
    blo
  }
}