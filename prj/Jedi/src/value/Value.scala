package value

import expression.{Expression, Identifier, Literal}
import system.{TypeException, UndefinedException}

import scala.collection.mutable

trait Value extends java.io.Serializable

class Notification(val msg: String) extends Value {
  override def toString = msg
}

object Notification {
  def apply(msg: String) = new Notification(msg)

  val OK = Notification("ok")
  val DONE = Notification("done")
  val UNDF = Notification("unspecified")
}

class Environment(var extension: Environment = null)
  extends mutable.HashMap[Identifier, Value] with Value {

  override def apply(name: Identifier): Value = {
    if (this.contains(name)) super.apply(name)
    else if (extension != null) extension.apply(name)
    else throw new UndefinedException(name)
  }
}

case class Number(val value: Double) extends Literal {
  def +(other: Number) = Number(this.value + other.value)

  def *(other: Number) = Number(this.value * other.value)

  def -(other: Number) = Number(this.value - other.value)

  def /(other: Number) = Number(this.value / other.value)

  def <(other: Number) = new Boole(this.value < other.value)

  def ==(other: Number) = new Boole(this.value == other.value)

  override def toString = value.toString
}

class Boole(val value: Boolean) extends Literal {
  def &&(other: Boole): Boole = Boole(other.value && value)

  def ||(other: Boole): Boole = Boole(other.value || value)

  def <(other: Boole): Boole = Boole(other.value < value)

  def !=(other: Boole): Boole = Boole(other.value != value)

  def ==(other: Boole): Boole = Boole(other.value == value)

  def ! = Boole(!value)

  override def toString = value.toString

}

object Boole {
  val FALSE = Boole(false)
  val TRUE = Boole(true)

  def apply(value: Boolean) = new Boole(value)
}

class Closure(val params: List[Identifier], val body: Expression, val defEnv: Environment) extends Value {
  override def toString = "lambda" + " " + params + " " + body

  def apply(args: List[Value]): Value = {
    val newEnv = new Environment(defEnv)
    if (params.length != args.length) throw new TypeException(params.toString + " != " + args.toString)
    for (i <- 0 until args.length) {
      newEnv.put(params(i), args(i))
    }
    body.execute(newEnv)
  }
}

class Variable(var value: Value) extends Value {
  override def toString = "var(" + value + ")"
}