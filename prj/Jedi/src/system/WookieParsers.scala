package system

import expression.{Block, Expression, Identifier, Lambda}

class WookieParsers extends EwokParsers {
  // block ::= "{" ~ expression ~ (";" ~ expression) * ~ "}" ^^ { make a Block }
  // lambda ::= "lambda' ~ parameters ~ expression ^ { make a lambda }
  // parameters ::= "(" ~ identifier* ~ ")" ^^ { make List[Identifier]}
  override def term: Parser[Expression] = lambda | block | super.term

  def block: Parser[Expression] = "{" ~ expression ~ rep(";" ~> expression) ~ "}" ^^ {
    case "{" ~ exp ~ Nil ~ "}" => Block(List(exp))
    case "{" ~ exp ~ exps ~ "}" => Block(exp :: exps)
  }

  def parameters: Parser[List[Identifier]] = "(" ~> opt(identifier ~ rep("," ~> identifier)) <~ ")" ^^ {
    case None => Nil
    case Some(e ~ Nil) => List(e)
    case Some(e ~ exps) => e :: exps
    case _ => Nil
  }

  def lambda: Parser[Expression] = "lambda" ~ parameters ~ expression ^^ {
    case "lambda" ~ params ~ body => Lambda(params, body)
  }
}
