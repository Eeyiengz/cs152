package system

import scala.util.parsing.combinator._
import expression._
import value._

/*
Ewok grammar:

   expression ::= declaration | conditional | disjunction
   declaration ::= "def"~identifier~"="~expression
   conditional ::= "if"~"("~expression~")"~expression~("else"~expression)?
   disjunction ::= conjunction~("||"~conjunction)*
   conjunction ::= equality~("&&"~equality)*
   equality ::= inequality~("=="~inequality)*
   inequality ::= sum ~ ("<" ~ sum)?
   sum ::= product ~ (("+" | "-") ~ product)*
   product ::= term ~ (("*" | "/") ~ term)*
   term ::= funcall | identifier | number | boole | "("~expression~")"
   funcall ::= identifier~operands
   operands ::= "("~(expression ~ (","~expression)*)? ~ ")"
   identifier ::= [a-zA-Z][a-zA-Z0-9]*
   number ::= [1-9][0-9]*("."[0-9]+)?
   boole ::= "true" | "false"

*/

class EwokParsers extends RegexParsers {

  def expression: Parser[Expression] = declaration | conditional | disjunction | failure("Invalid expression")

  def declaration: Parser[Declaration] = "def" ~ identifier ~ "=" ~ expression ^^ {
    case "def" ~ id ~ "=" ~ exp => Declaration(id, exp)
  }

  def conditional: Parser[Conditional] = "if" ~ "(" ~ expression ~ ")" ~ expression ~ opt("else" ~> expression) ^^ {
    case "if" ~ "(" ~ bool ~ ")" ~ doThis ~ None => Conditional(bool, doThis)
    case "if" ~ "(" ~ bool ~ ")" ~ doThis ~ Some(doThat) => Conditional(bool, doThis, doThat)
  }

  def disjunction: Parser[Expression] = conjunction ~ rep("||" ~> conjunction) ^^ {
    case con ~ Nil => con
    case con ~ cons => Disjunction(con :: cons)
  }

  def conjunction: Parser[Expression] = equality ~ rep("&&" ~> equality) ^^ {
    case eq ~ Nil => eq
    case eq ~ eqs => Conjunction(eq :: eqs)
  }

  def equality: Parser[Expression] = inequality ~ rep("==" ~> inequality) ^^ {
    case ineq ~ Nil => ineq
    case ineq ~ ineqs => FunCall(Identifier("equals"), ineq :: ineqs)
  }

  def inequality: Parser[Expression] = sum ~ opt(("<" | ">" | "!=") ~ sum) ^^ {
    case t ~ None => t
    case t ~ Some("<" ~ s) => FunCall(Identifier("less"), List(t, s))
    case t ~ Some(">" ~ s) => FunCall(Identifier("more"), List(t, s))
    case t ~ Some("!=" ~ s) => FunCall(Identifier("unequals"), List(t, s))
  }

  def sum: Parser[Expression] = product ~ rep(("+" | "-") ~ product ^^ {
    case "+" ~ p => p
    case "-" ~ p => FunCall(Identifier("sub"), List(Number(0), p))
  }) ^^ {
    case p ~ Nil => p
    case p ~ ps => FunCall(Identifier("add"), p :: ps)
  }

  def product: Parser[Expression] = term ~ rep(("*" | "/") ~ term ^^ {
    case "*" ~ t => t
    case "/" ~ t => FunCall(Identifier("div"), List(Number(1), t))
  }) ^^ {
    case t ~ Nil => t
    case t ~ ts => FunCall(Identifier("mul"), t :: ts)
  }

  def term: Parser[Expression] = funcall | boole | number | identifier | "(" ~> expression <~ ")"

  def funcall: Parser[Expression] = identifier ~ operands ^^ {
    case i ~ ops => FunCall(i, ops)
  }

  def operands: Parser[List[Expression]] = "(" ~ rep(expression) ~ ")" ^^ {
    case "(" ~ exps ~ ")" => exps
  }

  def identifier: Parser[Identifier] =
    """[a-zA-Z][a-zA-Z0-9]*""".r ^^ (someString => Identifier(someString))

  def number: Parser[Number] =
    """(\+|-)?(0|([1-9][0-9]*))(\.[0-9]+)?""".r ^^ (num => Number(num.toDouble + 0.0))

  def boole: Parser[Boole] = "true|false".r ^^ (bool => Boole(bool.toBoolean))
}