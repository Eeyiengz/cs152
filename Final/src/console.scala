
import scala.io._
import scala.util.parsing.combinator._

// TO DO: Complete this declaration:
object bexpParsers extends RegexParsers {

  def disjunction: Parser[Disjunction] = conjunction ~ opt("||" ~> disjunction) ^^ {
    case con ~ None => Disjunction(con)
    case con ~ Some(dis) => Disjunction(con, dis)
  }

  def conjunction: Parser[Conjunction] = boole ~ opt("&&" ~> conjunction) ^^ {
    case bool ~ None => Conjunction(bool)
    case bool ~ Some(con) => Conjunction(bool, con)
  }

  def boole: Parser[Boole] = "true|false".r ^^ { bool => Boole(bool.toBoolean) }
}

class SyntaxException(val result: Parsers#Failure = null) extends Exception("Syntax error")

object console {

  def execute(cmmd: String): String = {
    val tree = bexpParsers.parseAll(bexpParsers.disjunction, cmmd)
    tree match {
      case tree: bexpParsers.Failure => throw new SyntaxException(tree)
      case _ => {
        val exp = tree.get // get the expression from the tree
        val result = exp.execute // execute the expression
        result.toString // return string representation of result
      }
    }
  }

  def repl {
    var more = true
    var cmmd = ""
    while (more) {
      try {
        print("-> ")
        cmmd = StdIn.readLine
        if (cmmd == "quit") more = false
        else println(execute(cmmd))
      }
      catch {
        case e: SyntaxException => {
          println(e)
          println(e.result.msg)
          println("line # = " + e.result.next.pos.line)
          println("column # = " + e.result.next.pos.column)
          println("token = " + e.result.next.first)
        }
        case e: Exception => {
          println(e)
        }
      }
    }
    println("bye")
  }

  def main(args: Array[String]): Unit = {
    repl
  }

}

// OR
case class Disjunction(val con: Conjunction, val dis: Disjunction = null) {
  def execute: Boolean = {
    var disEx = false
    if (dis != null) disEx = dis.execute

    var conEx = con.execute
    conEx || disEx
  }
}

// AND
case class Conjunction(val bool: Boole, val con: Conjunction = null) {
  def execute: Boolean = {
    var conEx = true
    if (con != null) conEx = con.execute

    if (bool.bval) conEx else false
  }
}

case class Boole(val bval: Boolean) {
  def execute: Boolean = bval.booleanValue

  def apply(value: Boolean) = new Boole(value)
}