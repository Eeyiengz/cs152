object whhh {
  // problem 1
  def cond[T](test: T => Boolean, branch1: T => T, branch2: T => T): T => T = {
    def f(t: T) = {
      if (test(t)) branch1(t)
      else branch2(t)
    }
   
    f _
  }                                               //> cond: [T](test: T => Boolean, branch1: T => T, branch2: T => T)T => T

  val jump = cond((x: Int) => x % 2 == 0, (x: Int) => x * x, (y: Int) => y * y * y)
                                                  //> jump  : Int => Int = <function1>
  // = 27
  jump(3)                                         //> res0: Int = 27
  // = 16
  jump(4)                                         //> res1: Int = 16

  // problem 2
  def map[T](array: Array[T], test: T => Boolean, transform: T => T) {
    def helper(count: Int, result: Array[T]): Array[T] = {
      if (count >= array.size) result else if (test(result(count))) helper(count + 1, {
        result(count) = transform(result(count))
        result
      }) else helper(count + 1, result)

    }

    helper(0, array)
  }                                               //> map: [T](array: Array[T], test: T => Boolean, transform: T => T)Unit

  val nums = Array(-3, 2, 6, -4, 9, 0, -8, 9)     //> nums  : Array[Int] = Array(-3, 2, 6, -4, 9, 0, -8, 9)
  map(nums, (x: Int) => x < 0, (y: Int) => y * -1)
  // turns to Array(3, 2, 6, 4, 9, 0, 8, 9)
  nums                                            //> res2: Array[Int] = Array(3, 2, 6, 4, 9, 0, 8, 9)


  // problem 3
  def tester[T, S](f: T => S, inputs: Array[T], outputs: Array[S]): Int = {
    if(inputs.size != outputs.size) throw new Exception("Arrays not the same size!")
    def helper(count: Int, result: Int) : Int = {
      if(count >= inputs.size) result
      else if(f(inputs(count)) != outputs(count)) helper(count+1, result+1) else helper(count+1, result)
    }
    helper(0, 0)
  }                                               //> tester: [T, S](f: T => S, inputs: Array[T], outputs: Array[S])Int


  def isPower2(n: Int) = n % 2 == 0 && n / 2 % 2 == 0
                                                  //> isPower2: (n: Int)Boolean

  // = 2
  tester(isPower2 _, Array(2, 4, 6, 8, 10, 12, 32), Array(true, true, false, true, false, false, true))
                                                  //> res3: Int = 2

}