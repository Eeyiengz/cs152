object whhh {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(203); 
  // problem 1
  def cond[T](test: T => Boolean, branch1: T => T, branch2: T => T): T => T = {
    def f(t: T) = {
      if (test(t)) branch1(t)
      else branch2(t)
    }
   
    f _
  };System.out.println("""cond: [T](test: T => Boolean, branch1: T => T, branch2: T => T)T => T""");$skip(86); 

  val jump = cond((x: Int) => x % 2 == 0, (x: Int) => x * x, (y: Int) => y * y * y);System.out.println("""jump  : Int => Int = """ + $show(jump ));$skip(20); val res$0 = 
  // = 27
  jump(3);System.out.println("""res0: Int = """ + $show(res$0));$skip(20); val res$1 = 
  // = 16
  jump(4);System.out.println("""res1: Int = """ + $show(res$1));$skip(374); 

  // problem 2
  def map[T](array: Array[T], test: T => Boolean, transform: T => T) {
    def helper(count: Int, result: Array[T]): Array[T] = {
      if (count >= array.size) result else if (test(result(count))) helper(count + 1, {
        result(count) = transform(result(count))
        result
      }) else helper(count + 1, result)

    }

    helper(0, array)
  };System.out.println("""map: [T](array: Array[T], test: T => Boolean, transform: T => T)Unit""");$skip(48); 

  val nums = Array(-3, 2, 6, -4, 9, 0, -8, 9);System.out.println("""nums  : Array[Int] = """ + $show(nums ));$skip(51); 
  map(nums, (x: Int) => x < 0, (y: Int) => y * -1);$skip(51); val res$2 = 
  // turns to Array(3, 2, 6, 4, 9, 0, 8, 9)
  nums;System.out.println("""res2: Array[Int] = """ + $show(res$2));$skip(400); 


  // problem 3
  def tester[T, S](f: T => S, inputs: Array[T], outputs: Array[S]): Int = {
    if(inputs.size != outputs.size) throw new Exception("Arrays not the same size!")
    def helper(count: Int, result: Int) : Int = {
      if(count >= inputs.size) result
      else if(f(inputs(count)) != outputs(count)) helper(count+1, result+1) else helper(count+1, result)
    }
    helper(0, 0)
  };System.out.println("""tester: [T, S](f: T => S, inputs: Array[T], outputs: Array[S])Int""");$skip(58); 


  def isPower2(n: Int) = n % 2 == 0 && n / 2 % 2 == 0;System.out.println("""isPower2: (n: Int)Boolean""");$skip(115); val res$3 = 

  // = 2
  tester(isPower2 _, Array(2, 4, 6, 8, 10, 12, 32), Array(true, true, false, true, false, false, true));System.out.println("""res3: Int = """ + $show(res$3))}

}
